#!/usr/bin/env bash
 
composer update -n
composer install -n
./bin/console doctrine:schema:update --force --no-interaction
./bin/console doctrine:fixtures:load --no-interaction
 
exec "$@"